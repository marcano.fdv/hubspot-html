<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>HUBSPOT</title>
    <!--FONT-->
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Open+Sans:wght@300;400;600;700&display=swap" rel="stylesheet">
    <!--CUSTOM CSS-->
    <link rel="stylesheet" href="static/css/styles.css">
    <!--ICONS-->
    <link rel="stylesheet" type="text/css" href="statics/css/font/flaticon.css">
    <!--Flexbox-->
    <link rel="stylesheet" type="text/css" href="statics/css/flexboxgrid.css">
</head>
<body   >

    <header class="container-cont">
            <nav id="header-1" class="dis-flex just-cont-betw ">
                <div class="f-family dis-flex al-items-center">
                    <ul class="dis-flex f-family f-12px ">
                        <li> <a class="a  marg-left f-color flaticon-globe" href="/">Español</a> </li>
                        <li> <a class="a f-color flaticon-user" href="/">Contactar con ventas</a> </li>
                    </ul>
                </div>
               <div class="f-family dis-flex al-items-center">
                    <ul class="dis-flex f-family f-12px">
                        <li> <a class="a flaticon-search" href="/"></a> </li>
                        <li> <a class="a f-color" href="/">Iniciar sesión</a> </li>
                        <li> <a class="a f-color" href="/">Atención al Cliente</a> </li>
                        <li> <a class="a f-color" href="/">Sobre Nosotros</a> </li>
                    </ul>
                </div>
            </nav>
        <nav id="header-2" class="dis-flex just-cont-betw al-items-center">
            <div class="dis-flex">
                <a href="/"><img class="logo" src="./statics/Images/guidelines_the-logo.svg" alt="logo hubspot"></a>
                <ul class=" dis-flex al-items-center f-family f-17px">
                    <li><a class="a f-color" href="/">Software</a></li>
                    <li><a class="a f-color" href="/">Precios</a></li>
                    <li><a class="a f-color" href="/">Recursos</a></li>
                </ul>
            </div>
            <div >
                <button  class="button-orange button-small b-radius" type="button">Obtener Hubspot Gratis</button>
            </div>
        </nav>
    </header>
    <hr class="box-shadow">

</body>