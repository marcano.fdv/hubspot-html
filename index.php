<?php include ("./partials/header.php") ?>

<!--Styles-->
<link rel="stylesheet" href="statics/css/styles.css">

<div>
    <div >
        <div >
            <section id="section-1" >
                <div class="container-cont">
                    <div id="cont-1"  class=" dis-flex flex-column just-cont-center al-items-right" >
                        <h1 class="f-family f-color f-40px padd-bottom-10" >Ahora puedes <br> crecer mejor </h1>
                        <p class="f-family f-color f-17px padd-bottom-5" > Obtén nuestro software de marketing, ventas y servicio de
                            atención al cliente para potenciar el crecimiento de tu empresa.
                            Porque cuando tu empresa crece, tus clientes crecen. </p>
                        <button class="button-orange button-big button-gen  b-radius f-family f-17px " type="button">Obten Hubspot gratis</button>
                        <p class="f-color f-family f-12px f-weight-300 padd-top-5">Comienza de manera gratuita y aumenta tu software a
                            medida que creces.</p>
                    </div>

                </div>
            </section>
            <section id="section-2" class=" container-cont dis-flex flex-column just-cont-center al-items-center padd-top-5 ">
                <h2 class="f-family f-color f-35px f-weight-700 padd-bottom-2"> La plataforma CRM perfecta para toda tu compañía </h2>
                <div class="width-70vh padd-bottom-2">
                    <p class="f-family f-color f-17px f-weight-300"> La plataforma CRM de HubSpot incluye todas
                        las herramientas e integraciones que necesitas para marketing,
                        ventas, gestión de contenido y atención al cliente.
                        Cada producto es potente por sí solo; en combinación, son inigualables.</p>
                </div>

                <div class="dis-flex al-items-center just-cont-center padd-bottom-2">
                    <button  class="f-family f-17px f-weight-400 button-orange button-gen  button-small b-radius marg-right" type="button">Usar CRM gratis</button>
                    <button  class="f-family f-17px f-weight-400 button-white button-small button-gen   b-radius" type="button">Probar CRM prémium</button>
                </div>
                <div class="dis-flex flex-wrap just-cont-center al-items-center ">
                    <div class="card back-white width-40vh dis-flex flex-column just-cont-center margin-20 padd-50px">
                        <img class="logo center"  src="./statics/Images/guidelines_the-logo.svg" alt="Logo Marketing Hub">
                        <p class="f-family f-color f-12px text-center padd-top-5" >Un software de marketing que te ayudará a aumentar el tráfico,
                            convertir más visitantes en clientes y ejecutar campañas de inbound marketing completas.</p>
                        <h3 class="f-family f-color f-weight-300 f-17px padd-top-5 text-right">CARACTERÍSTICAS POPULARES</h3>
                        <ul class="padd-top-5 dis-flex flex-column al-items-right just-cont-center padd-bottom-5">
                            <li class="f-color f-family f-weight-300 f-12px padd-bottom-2 flaticon-checked">Generación de leads</li>
                            <li class="f-color f-family f-weight-300 f-12px padd-bottom-2 flaticon-checked">Automatización del marketing</li>
                            <li class="f-color f-family f-weight-300 f-12px flaticon-checked">Analíticas</li>
                        </ul>
                        <button  class="button-orange button-small button-gen b-radius center" type="button">Comenzar</button>
                    </div>

                </div>
            </section>
            <section id="section-3" >
                <div class=" container-cont dis-flex flex-column just-cont-center al-items-center">
                    <div class="width-70vh  padd-top-10">
                        <h1 class="f-family f-color-w f-25px f-weight-700 text-center">Aprende y crece de la mano de un
                            reconocido equipo de asistencia técnica y con el apoyo de una creciente comunidad</h1>
                        <p class="f-family f-color-w f-20px f-weight-300 text-center padd-top-5">No estarás solo. Domina la metodología
                            inbound y saca el máximo partido a tus herramientas con el
                            valioso apoyo del equipo de asistencia técnica de HubSpot y una
                            comunidad de miles de profesionales del marketing y las ventas.</p>
                    </div>
                    <div class="row padd-top-5 padd-bottom-10">
                        <div class="col-lg-3 col-md col-xs padd-40px dis-flex flex-column al-items-center text-center just-cont-even border-right border-bottom">
                            <img class="img-logo-150" src="./statics/Images/homepage_community_agency.svg" alt="Manos">
                            <h2 class="f-family f-color-w f-weight-700 f-40px ">Más de 150</h2>
                            <p class="f-family f-color-w f-weight-400 f-17px">Grupos de Usuarios de HubSpot</p>
                        </div>
                        <div class="col-lg-3 col-md col-xs padd-40px dis-flex flex-column al-items-center text-center just-cont-even border-right border-bottom">
                            <img class="img-logo-150" src="./statics/Images/homepage_community_blog.svg" alt="Hubspot Blogs">
                            <h2 class="f-family f-color-w f-weight-700 f-40px ">Más de 7 mill.</h2>
                            <p class="f-family f-color-w f-weight-400 f-17px">de visitas mensuales</p>
                        </div>
                        <div class="col-lg-3 col-md col-xs padd-40px dis-flex flex-column al-items-center text-center just-cont-even border-right border-bottom">
                            <img class="img-logo-150" src="./statics/Images/homepage_community_academy.svg" alt="Hubspot Academy">
                            <h2 class="f-family f-color-w f-weight-700 f-40px ">Más de 330.000</h2>
                            <p class="f-family f-color-w f-weight-400 f-17px">profesionales certificados</p>
                        </div>
                        <div class="col-lg-3 col-md col-xs padd-40px dis-flex flex-column al-items-center text-center just-cont-even border-bottom">
                            <img class="img-logo-150" src="./statics/Images/homepage_community_inbound.svg" alt="INBOUND">
                            <h2 class="f-family f-color-w f-weight-700 f-40px ">Más de 26.000</h2>
                            <p class="f-family f-color-w f-weight-400 f-17px">asistentes registrados</p>
                        </div>
                        <div class="col-lg-3 col-md col-xs padd-40px dis-flex flex-column al-items-center text-center just-cont-even border-right">
                            <img class="img-logo-150" src="./statics/Images/HubSpot-App-Marketplace-White-1.webp" alt="Hubspot App marketplace">
                            <h2 class="f-family f-color-w f-weight-700 f-40px ">Más de 650</h2>
                            <p class="f-family f-color-w f-weight-400 f-17px">integraciones</p>
                        </div>
                        <div class="col-lg-3 col-md col-xs padd-40px dis-flex flex-column al-items-center text-center just-cont-even border-right">
                            <img class="img-logo-150" src="./statics/Images/homepage_community_globe.svg" alt="Idiomas">
                            <h2 class="f-family f-color-w f-weight-700 f-40px ">7</h2>
                            <p class="f-family f-color-w f-weight-400 f-17px">Idiomas</p>
                        </div>
                        <div class="col-lg-3 col-md col-xs padd-40px dis-flex flex-column al-items-center text-center just-cont-even border-right">
                            <img class="img-logo-150" src="./statics/Images/homepage_community_followers.svg" alt="seguidores">
                            <h2 class="f-family f-color-w f-weight-700 f-40px ">Más de 2,6 mill.</h2>
                            <p class="f-family f-color-w f-weight-400 f-17px">de seguidores</p>
                        </div>
                        <div class="col-lg-3 col-md col-xs padd-40px dis-flex flex-column al-items-center text-center just-cont-even">
                            <img class="img-logo-150" src="./statics/Images/Lists.svg" alt="Clientes">
                            <h2 class="f-family f-color-w f-weight-700 f-40px ">100.000</h2>
                            <p class="f-family f-color-w f-weight-400 f-17px">clientes</p>
                        </div>
                    </div>

                </div>

        </section>
            <section id="section-4" class=" container-cont dis-flex just-cont-center al-items-center padd-top-5">
                <div class="dis-flex flex-column just-cont-center al-items-right width-50vh">
                    <h1 class="f-family f-color f-weight-600 f-25px">100.000</h1>
                    <p class="f-family f-color f-weight-300 f-17px">clientes en más de <strong>120</strong>
                        países confían en HubSpot para su crecimiento </p>
                </div>
                <div class="dis-grid dis-grid-4 width-50vh">
                    <img class="margin-20 dis-flex al-items-center just-cont-center img-logo" src="./statics/Images/shopify.webp" alt="Shopify">
                    <img class="margin-20 dis-flex al-items-center just-cont-center img-logo" src="./statics/Images/usabilia.webp" alt="Usabilla">
                    <img class="margin-20 dis-flex al-items-center just-cont-center img-logo" src="./statics/Images/tufts.webp" alt="Tufts">
                    <img class="margin-20 dis-flex al-items-center just-cont-center img-logo" src="./statics/Images/iadvize.webp" alt="iAdvize">
                    <img class="margin-20 dis-flex al-items-center just-cont-center img-logo" src="./statics/Images/mention.webp" alt="Mention">
                    <img class="margin-20 dis-flex al-items-center just-cont-center img-logo" src="./statics/Images/care.webp" alt="Care.com">
                    <img class="margin-20 dis-flex al-items-center just-cont-center img-logo" src="./statics/Images/scrive.webp" alt="Scrive">
                    <img class="margin-20 dis-flex al-items-center just-cont-center img-logo" src="./statics/Images/Randstad-logo_main_99ACC2.webp" alt="Randstad">
                </div>
            </section>
            <section id="test-section" class="container-cont">
                <div class="row middle-lg between-lg">
                    <div class="col-lg-2 col-mg-4 col-xs-6">
                        <img class="margin-20 dis-flex al-items-center just-cont-center img-logo" src="./statics/Images/shopify.webp" alt="Shopify">
                    </div>
                    <div class="col-lg-4 col-md-8 col-xs-6">
                        <h1 class="f-family f-color f-weight-600 f-25px">100.000</h1>
                        <p class="f-family f-color f-weight-300 f-17px">clientes en más de <strong>120</strong>
                            países confían en HubSpot para su crecimiento </p>
                    </div>
                    <div class="col-lg-2 col-md-4 col-xs-6 ">
                        <div class="box">
                            <img class="margin-20 dis-flex al-items-center just-cont-center img-logo" src="./statics/Images/shopify.webp" alt="Shopify">
                        </div>
                    </div>
                    <div class="col-lg-2 col-md-4 col-xs-6">
                        <img class="margin-20 dis-flex al-items-center just-cont-center img-logo" src="./statics/Images/shopify.webp" alt="Shopify">
                    </div>
                    <div class="col-lg-2 col-md-4 col-xs-6">
                        <img class="margin-20 dis-flex al-items-center just-cont-center img-logo" src="./statics/Images/shopify.webp" alt="Shopify">
                    </div>
                </div>


            </section>
    </div>
</div>


<?php include ("./partials/footer.php") ?>
